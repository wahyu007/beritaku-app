import React, { useState } from "react";

import Header from "./components/Header";
import Navbar from "./components/Navbar";
import TaskList from "./components/TaskList";
const App: React.FC = () => {
  const [category, setCategory] = useState('general');
  return (
    <main className="container relative bg-gray-100 mx-auto max-w-lg px-4 py-2 box-border min-h-screen">
      <Header />
      <Navbar setCategory={setCategory} onSelected={category} />
      <TaskList category={category} />
    </main>
  );
};

export default App;
