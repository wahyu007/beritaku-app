import axios from "axios";
import * as dotenv from 'dotenv';
import { News } from "../../types/todos.type";

dotenv.config();
export const getTodos = async (category: string): Promise<News> => {
  try {
    const res = await axios.get(`https://newsapi.org/v2/everything?q=${category}&apiKey=bcdea46ea22c49d281937c8ae0dc731b`);

    return res.data;
  } catch (error) {
    throw Error(error);
  }
};
