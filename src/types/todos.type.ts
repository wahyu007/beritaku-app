export interface News {
  status: string,
  totalResults: number,
  articles: Articles[]
}

interface Articles{
  source: sourceA,
  author: string,
  title: string,
  description: string,
  url: string,
  urlToImage: string,
  publishedAt: string,
  content: string
}

interface sourceA {
  id: string,
  name: string
}