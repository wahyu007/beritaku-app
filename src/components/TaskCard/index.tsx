import React from "react";
import Moment from 'react-moment';

type props = {
  name: string,
  author: string,
  url: string,
  title: string,
  image: string,
  content: string,
  date: string
};

const TaskCard: React.FC<props> = ({ name, author, url, title, image, content, date }) => {
  return (
      <div className="justify-center items-center relative rounded shadow-lg mb-5 bg-white">
        <div className="relative overflow-hidden text-white">
          <img
            className="relative z-auto w-full object-cover object-center rounded-t-lg"
            style={{height: '200px'}}
            src={image}
            alt=""
          />
          <h3 
            className="absolute z-20 leading-6 font-bold bottom-0 " 
            style={{ paddingLeft: '3%', fontSize: '1.2rem', backgroundColor: 'rgba(0, 0, 0, 0.2)'}}
          >
            {title}
          </h3>
        </div>
        <div className="relative bg-blue-100 ">
          <div className="py-5 px-3">
            <div className="text-red-600 text-sm font-semibold flex mb-1">
              <Moment format="ddd, MMMM DD, YYYY">{date}</Moment>
            </div>
            <div className="text-sm font-bold mb-5">
              <p className="mb-2 text-xs leading-3">
              {author} | {name}
              </p>
            </div>
            <p className="leading-5 text-xs">
              {content.substring(0, 100)} ...
            </p>
            <div className="mt-5 flex flex-row-reverse items-center text-blue-500">
              <a
                href={url}
                className="flex items-center"
              >
                <p className="mr-4 ">Read more</p>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="14.125"
                  height="13.358"
                  viewBox="0 0 14.125 13.358"
                >
                  <g transform="translate(-3 -3.293)">
                    <path
                      id="Path_7"
                      data-name="Path 7"
                      d="M14.189,10.739H3V9.2H14.189L9.361,4.378l1.085-1.085,6.679,6.679-6.679,6.679L9.361,15.566Z"
                      fill="#4299e1"
                      fill-rule="evenodd"
                    ></path>
                  </g>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
  );
};

export default TaskCard;
