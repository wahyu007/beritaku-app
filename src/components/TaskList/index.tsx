import React from "react";

import TaskCard from "../TaskCard";
import Loading from "../Loading";

import { useQuery } from "react-query";
import { getTodos } from "../../api/getNews";

type Props = {
  category: string
}

const TaskList: React.FC<Props> = ({ category }) => {
  const { isLoading, isError, error, data } = useQuery(["news", category], () => getTodos(category));

  if (isLoading) {
    return (
      <Loading />
    )
  }

  if(isError){
      return (
        <div>error</div>
      )
  }

  return (
    <section className="overflow-x-hidden overflow-y-auto h-taskList rounded">
      {data?.articles.map((news) => {
        return (
          <TaskCard name={news.source.name} author={news.author} url={news.url}  title={news.title} date={news.publishedAt} image={news.urlToImage} content={news.description}/>
        )
      })}
    </section>
  );
};

export default TaskList;
