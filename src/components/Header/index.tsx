import React from "react";

import LogoSvgComponent from "../../assets/svg/logoNews";

const Header: React.FC = () => {
  return (
    <nav className="flex justify-center items-center w-full box-border bg-gray-100 mb-2">
      <LogoSvgComponent height="24px" width="24px"></LogoSvgComponent>
      <h3 className=" text-xl font-bold ml-1">Beritaku</h3>
    </nav>
  );
};

export default Header;
