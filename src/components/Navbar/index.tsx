import React from "react";

const arrayHeader = ["general", "business", "entertainment", "health", "science", "sports", "technology"]

type Props = {
  onSelected: string,
  setCategory: (item: string) => void;
}

const Navbar: React.FC<Props> = ({setCategory, onSelected}) => {
  const handleNavbar = (item: string) => {
    setCategory(item)
  }
  return (
    <nav className="flex overflow-x-auto overflow-y-hidden mb-5 text-white">
      {
        arrayHeader.map(item => {
          if(onSelected === item){
            return(
              <div className="w-13 mr-4 bg-red-700 px-2 py-1 rounded-lg font-semibold box-border text-sm">
                {item}
              </div>
            )
          } else if(onSelected !== item){
            return(
              <a href={`#${item}`}>
                <div className="w-13 mr-4 bg-red-500 px-2 py-1 rounded-lg font-semibold box-border text-sm" onClick={() => handleNavbar(item)}>
                  {item}
                </div>
              </a>
            )
          }
        })
      }      
    </nav>
  );
};

export default Navbar;
